#!/python3

"""
    Setup Tools for distribution
"""

from datetime import datetime
import os
import setuptools

with open("README.md", "r") as obj:
    long_description = obj.read()

version_tag = datetime.utcnow().strftime("%Y.%-m.%-d")

if 'CI_BUILD_ID' in os.environ:
    # Published builds must have a unique version
    version_tag = datetime.utcnow().strftime("%Y.%-m.%-d.%H%M")
    # Special: If this is a GitLabCI run from a "tag" event, use that tag as the version
    version_tag = os.environ.get('CI_BUILD_TAG', version_tag)

setuptools.setup(
    name="test_py_package",
    version=version_tag,
    author="Jason De Arte",
    author_email="Jason.De.Arte@gmail.com",
    description="Example python package for testing",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jasondearte/testpypackage",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: NONE",
        "Operating System :: OS Independent"
    ],
    python_requires='>=3.9',
    install_requires=['requests']
)
