#!/python3

"""
    public interfaces
"""

from .code import Example

__all__ = [
    'Example'
]
