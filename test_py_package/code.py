#!/python3

"""
    Just a boring example class to export as part of our package
"""


class Example:
    """
    Example do nothing class
    """
    def __init__(self, pick_a_number: int=0):
        """
        c-tor
        """
        self.reset(pick_a_number)

    def reset(self, number):
        """
        set the number
        """
        self.number=number

    def test(self):
        """
        documentation on test
        """
        print(f"This is a test of example, your number is {self.number}")


if __name__ == '__main__':
    # This is where I would write any local unit tests
    pass
