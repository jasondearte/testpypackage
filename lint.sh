#!/usr/bin/env sh

echo "=============="
echo "pylint of *.py"
pylint *.py

echo "pylint of package source in test_py_package"
pylint test_py_package
